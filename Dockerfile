FROM ubuntu:20.04

RUN apt-get update \
    && apt-get install -y software-properties-common \
    && apt-add-repository ppa:ondrej/php
RUN apt-get update \
    && apt-get install -y \
        build-essential \
        php8.0 \
        php8.0-dev

COPY ./build.sh /var/data/build.sh

WORKDIR /var/data
