# php-extension-demo

Simple demo how to write your first PHP 8.0 extension.

Demo is used on my blog entry written in Polish.

# How to use

0. You must have Docker on your machine.
1. Clone this repository.
2. Build extension with command:
    ```shell
    docker run -v "${PWD}/src/config.m4:/var/data/config.m4" -v "${PWD}/src/custom_sum.c:/var/data/custom_sum.c" -v "${PWD}/src/custom_sum.h:/var/data/custom_sum.h" -v "${PWD}/build:/var/data/modules"  php-extension bash /var/data/build.sh
    ```
3. Test extension with command:
    ```shell
    docker run -v "${PWD}/build:/var/data/modules" php-extension bash -c 'cp /var/data/modules/custom_sum.so $(php-config --extension-dir)/custom_sum.so && php -d extension=custom_sum.so -r \"echo customSum(6.1, 3.8);\"'
    ```
4. Have fun!
