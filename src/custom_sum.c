#include <php.h>
#include "custom_sum.h"

ZEND_BEGIN_ARG_INFO_EX(arginfo_customSum, 0, 0, 2)
    ZEND_ARG_INFO(0, firstNumber)
    ZEND_ARG_INFO(0, secondNumber)
ZEND_END_ARG_INFO()

zend_function_entry extension_functions[] = {
    PHP_FE(customSum, arginfo_customSum)
    {NULL, NULL, NULL}
};

zend_module_entry custom_sum_module_entry = {
    STANDARD_MODULE_HEADER,
    EXTENSION_NAME,
    extension_functions,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    EXTENSION_VERSION,
    STANDARD_MODULE_PROPERTIES
};

ZEND_GET_MODULE(custom_sum)

static double customSum(double firstNumber, double secondNumber)
{
    return firstNumber + secondNumber;
}

PHP_FUNCTION(customSum) {
    double firstNumber;
    double secondNumber;

    ZEND_PARSE_PARAMETERS_START(2,2)
        Z_PARAM_DOUBLE(firstNumber);
        Z_PARAM_DOUBLE(secondNumber);
    ZEND_PARSE_PARAMETERS_END();

    RETURN_DOUBLE(customSum(firstNumber, secondNumber));
}
